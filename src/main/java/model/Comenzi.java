package model;

public class Comenzi {
    private String numeClient;
    private String numeProdus;
    private int cantitate;

    public  Comenzi(){

    }

    public Comenzi(String numeClient, String numeProdus, int cantitate) {
        this.numeClient = numeClient;
        this.numeProdus = numeProdus;
        this.cantitate = cantitate;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getNumeProdus() {
        return numeProdus;
    }

    public void setNumeProdus(String numeProdus) {
        this.numeProdus = numeProdus;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

}