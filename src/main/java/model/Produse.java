package model;

import java.beans.PropertyDescriptor;

public class Produse {
    private int id;
    private String nume;
    private int cantitate;
    private float pret;

    public Produse(){

    }

    public Produse(String nume, int cantitate, float pret) {
        this.nume = nume;
        this.cantitate = cantitate;
        this.pret = pret;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }


}
