package model;

public class ComandaProdus {
    private int idClient;
    private int idProdus;

    public ComandaProdus(){

    }

    public ComandaProdus(int idClient, int idProdus) {
        this.idClient = idClient;
        this.idProdus = idProdus;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

}