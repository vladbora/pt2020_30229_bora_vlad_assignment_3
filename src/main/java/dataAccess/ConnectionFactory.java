package dataAccess;

import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVE = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/assigment3bd";
    private static final String USER = "root";
    private static final String PASS = "root";

    // camp care urmeaza pattern-ul Singleton
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    // constructor
    private ConnectionFactory(){
        try {
            Class.forName(DRIVE);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // creaza conexiunea cu baza de date
    private Connection createConnection(){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            System.out.println("Eroare nu a mers conexiunea la baza de date");
        }
        return connection;
    }

    /**
     * este folosit ca si getter
     * @return returneaza conexiunea la baza de date
     */
    public static  Connection getConnection(){
        return  singleInstance.createConnection();
    }

    /**
     * inchide conexiunea la baza de date
     * @param connection se foloseste pentru a inchide parea de connection
     */
    public  static  void close(Connection connection){
        if (connection != null)
        {
            try { connection.close();
            } catch (SQLException e) {

            }
        }
    }

    /**
     * metoda folosita pentru a inchide conexiunea la baza de date
     * @param statement in aceasta metoda se inchide statment-ul
     */
    public  static  void close(Statement statement){
        if (statement != null){
            try {
                statement.close();
            } catch (SQLException e) {

            }
        }
    }

    /**
     * se foloseste pentru a inchide conexiunea
     * @param resultSet este folosit pentru inchiderea partii de resultSet
     */
    public static  void close(ResultSet resultSet){
        if (resultSet != null){
            try {
                resultSet.close();
            } catch (SQLException e) {

            }
        }
    }
}
