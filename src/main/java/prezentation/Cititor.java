package prezentation;

import business.*;
import model.*;

import java.io.*;
import java.util.*;

public class Cititor {

    private String fileName;
    private int nrPdfClienti, nrPdfProduse, nrPdfComenzi;

    public Cititor(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private boolean incearcaCazuri(String linie){
        if (linie.equals("Report client")){
            ClientiDao clientiDao = new ClientiDao();
            nrPdfClienti++;
            List<Clienti> clientis = clientiDao.selectAll(nrPdfClienti);
            return true;
        }
        if (linie.equals("Report product")){
            ProduseDao produseDao = new ProduseDao();
            nrPdfProduse++;
            produseDao.selectAll(nrPdfProduse);
            return true;
        }
        if (linie.equals("Report order")){
            ComenziDao comenziDao = new ComenziDao();
            nrPdfComenzi++;
            comenziDao.selectAll(nrPdfComenzi);
            return true;
        }
        return false;
    }

    private boolean insereazaClient(String[] valori, String[] date){
        if (valori[0].equals("Insert client")) {
            ClientiDao abstractDao = new ClientiDao();
            Clienti client = new Clienti(date[0], date[1]);
            abstractDao.insert(client);
            return true;
        }
        return  false;
    }

    private boolean insereazaComanda(String[] valori, String[] date){
        if (valori[0].equals("Order")) {
            ComenziDao abstractDao = new ComenziDao();
            ProduseDao produseDao = new ProduseDao();
            ClientiDao clientiDao = new ClientiDao();
            int cantitate = Integer.parseInt(date[2]);
            Comenzi comenzi = new Comenzi(date[0], date[1], cantitate);
            Produse produs = produseDao.select("nume", date[1]);
            if (cantitate > produs.getCantitate()) {
                Scriitor<Comenzi> scriitor = new Scriitor<>();
                scriitor.scrieText("Nu avem destule produse pentru dvs. domnule/doamna " + date[0], "NuAvem" + date[0]);
                return true;
            }
            abstractDao.insert(comenzi);
            produseDao.update("cantitate", produs.getCantitate() - cantitate, produs);
            Clienti client = clientiDao.select("nume", date[0]);
            ComandaProdus comandaProdus = new ComandaProdus(client.getId(), produs.getId());
            Scriitor<Comenzi> scriitor = new Scriitor<>();
            scriitor.scrieText("Comanda pentru " + date[0] + " contine un nr de " + date[2] + " " + date[1] +" si costa "  + produs.getPret() * cantitate
                    , "Bill" + date[0] + " " + date[1]);
            return true;
        }
        return false;
    }

    private boolean insereazaProdus(String[] valori, String[] date){
        if (valori[0].equals("Insert product")) {
            ProduseDao abstractDao = new ProduseDao();
            int cantitate = Integer.parseInt(date[1]);
            float pret = Float.parseFloat(date[2]);
            Produse produse = new Produse(date[0], cantitate, pret);
            Produse produs = abstractDao.select("nume", date[0]);
            if ( produs == null) {
                abstractDao.insert(produse);
            }
            else{
                abstractDao.update("cantitate", produs.getCantitate() + cantitate,produs);
            }
            return true;
        }
        return  false;
    }

    private boolean delete(String[] valori, String[] date){

        if (valori[0].equals("Delete client")) {
            ClientiDao clientiDao = new ClientiDao();
            Clienti clienti = clientiDao.select("nume", date[0]);
            ComenziDao comenziDao = new ComenziDao();
            while(true){
                Comenzi comenzi = comenziDao.select("numeClient", date[0]);
                if (comenzi == null)
                    break;
                comenziDao.delete(comenzi);
            }
            clientiDao.delete(clienti);
            return true;
        }
        if (valori[0].equals("Delete Product")) {
            ProduseDao produseDao = new ProduseDao();
            Produse produse = produseDao.select("nume", date[0]);
            produseDao.delete(produse);
            return true;
        }
        return false;
    }

    private void incearcaCazuriSplit(String linie){
        String[] valori = linie.split(": ");
        String[] date = valori[1].split(", ");
        if (insereazaClient(valori, date)) {
            return;
        }
        if (insereazaComanda(valori, date)) {
            return;
        }
        if (insereazaProdus(valori, date)){
            return;
        }
        delete(valori, date);
    }

    /**
     * iteraza fisierul de intrare si parseaza informatia
     */
    public void incepeCitirea(){
        try {
            File f = new File(fileName);
            Scanner s = new Scanner(f);
            while(s.hasNextLine()){
                String linie = s.nextLine();
                if (incearcaCazuri(linie) == false) {
                    incearcaCazuriSplit(linie);
                }
            }
        }
        catch(FileNotFoundException e){
        }
    }
}
