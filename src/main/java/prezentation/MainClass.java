package prezentation;

public class MainClass {

    /**
     * inceputul programului
     * @param args se cere numele/path-ul catre fisierul in case se vor regasi datele de intrare
     */
    public static void main(String[] args)
    {
        Cititor reader = new Cititor(args[0]); //initializez un cititor pentru a incepe citirea
        reader.incepeCitirea();
    }

}
