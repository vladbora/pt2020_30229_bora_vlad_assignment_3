package prezentation;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Stream;


public class Scriitor <T>{

    /**
     *o functie care genereaza o factura
     * @param text este text-ul din factura
     * @param titlu este titlul facturii
     */
    public void scrieText(String text, String titlu) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(titlu + ".pdf"));

            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);
            Chunk chunk = new Chunk(text, font);

            try {
                document.add(chunk);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            document.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    // o functie care adauga un rand la o tabela
    private void addRows(PdfPTable table, T object) {
        for (Field field : object.getClass().getDeclaredFields())
        {
            try {
                field.setAccessible(true);
                Object value = field.get(object);
                table.addCell(value.toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }

    // o functie care adauga un header la o tabela
    private void addTableHeader(PdfPTable table, List<T> dePrintat) {
        String[] strings = new String[dePrintat.get(0).getClass().getDeclaredFields().length];
        int i = 0;

        for (Field field : dePrintat.get(0).getClass().getDeclaredFields())
        {
            strings[i] = field.getName();
            i++;
        }
        Stream.of(strings)
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * o functie care creaza un raport in forma de tabel
     * @param dePrintat obiectele care vor fi afisate in tabel
     * @param numePdf numele pdf-ului creat
     */
    public void scrieTabel(List<T> dePrintat, String numePdf){
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream( numePdf +".pdf"));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        document.open();

        PdfPTable table = new PdfPTable(dePrintat.get(0).getClass().getDeclaredFields().length);
        addTableHeader(table, dePrintat);

        for (T object : dePrintat) {
            addRows(table, object);
        }

        try {
            document.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

}
