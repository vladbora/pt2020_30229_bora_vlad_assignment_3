package business;

import dataAccess.ConnectionFactory;
import prezentation.Scriitor;

import java.beans.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

public abstract class AbstractDao <T>{

    // numele clasei curente
    protected final Class<T> type;

    // un constructor care ia numele clasei curente
    public AbstractDao(){
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    // creaza queri-ul folosit pentru inserare
    private String createInsertQuery(T object) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into " + object.getClass().getSimpleName() + "(");

        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (!field.getName().equals("id")) {
                sb.append(field.getName() + ",");
            }
        }
        sb.setLength(sb.length() - 1);
        sb.append(") VALUES (");
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            if (!field.getName().equals("id")) {
                try {
                    value = field.get(object);
                    sb.append( "\"" + value +  "\",");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        sb.setLength(sb.length() - 1);
        sb.append(");");
        return sb.toString();
    }

    /**
     * insereaza un obiect in baza de date
     * @param object obiectul care va fi inserat in baza de date
     */
    public void insert(T object){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery(object);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    // creaza un query de select
    private String createSelectQuery(String parametru, String parametruValue){
        StringBuilder sb = new StringBuilder();
        sb.append("Select * from " + type.getSimpleName());
        sb.append(" where " + parametru + "=\"" + parametruValue +"\";");
        return sb.toString();
    }

    /**
     * transforma un resultSet intr-o lista generica de obiecte
     * @param resultSet datele de intrare care vor fi prelucrate
     * @return lista generica de obiecte
     */
    private List<T> getObject(ResultSet resultSet){
        List<T> list = new ArrayList<T>();
            try {
                    while(resultSet.next()) {
                        T instance = type.newInstance();
                        for (Field field : type.getDeclaredFields()){
                            Object value = resultSet.getObject(field.getName());
                            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                            Method method = propertyDescriptor.getWriteMethod();
                            method.invoke(instance, value);
                        }
                        list.add(instance);
                    }
                } catch (SQLException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
            } catch (InstantiationException e) {
            } catch (IntrospectionException e) {
            } catch (InvocationTargetException e) {
            }
        return  list;
    }

    /**
     * un select dupa un anumit camp cu o valare
     * @param parametru numele campului dupa care vrem sa cautam
     * @param parametruValue valoarea parametrului pe care o cautam
     * @return obiectul returnat de select
     */
    public T select(String parametru, String parametruValue){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(parametru, parametruValue);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            List<T> produse = getObject(resultSet);
            if (produse.size() > 0)
                return produse.get(0);
            return  null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // creaza query-ul de update pe care il vom executa
    private String createUpdateQuery(String fieldName, int value, T object){
        StringBuilder sb = new StringBuilder();
        sb.append("Update "+ type.getSimpleName() + " set ");
        sb.append(fieldName +"=\"" + value +"\" where " );
        for (Field field : type.getDeclaredFields()){
            field.setAccessible(true);
            Object fieldValue;
            try {
                fieldValue = field.get(object);
                sb.append( field.getName() + "=\"" + fieldValue +  "\" and ");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.setLength(sb.length() - 5);
        return sb.toString();
    }

    /**
     * executa un query de update
     * @param fieldName numele field-ului la care vrem sa facem update
     * @param value valoarea nou a field-ului din baza de date pe careia vrem sa ii facem update
     * @param object reprezinta locul in baza de date unde dorim sa facem update
     */
    public void update(String fieldName, int value, T object){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery(fieldName, value, object);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // creaza text-ul pentru query-ul de delete
    private String createDeleteQuery(T object){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM " +  type.getSimpleName() + " where ");
        for (Field field : type.getDeclaredFields()){
            field.setAccessible(true);
            Object fieldValue;
            try {
                fieldValue = field.get(object);
                sb.append( field.getName() + "=\"" + fieldValue +  "\" and ");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.setLength(sb.length() - 5);
        return sb.toString();
    }

    /**
     * deletes an object from the database
     * @param object the object to be deleted from the dabase
     */
    public void delete(T object){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery(object);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * o functie care ia toate elementele dintr-o tabela si creaza un report
     * @param numar numarul pdf-ului creat
     * @return o lista cu toate elementele de un tip pe care le avem la un moment dat
     */
    public List<T> selectAll(int numar){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM " + type.getSimpleName();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            List<T> produse = getObject(resultSet);
            Scriitor<T> scriitor = new Scriitor();
            if (produse.size() == 0)
                return null;
            scriitor.scrieTabel(produse, type.getSimpleName() + numar);
            if (produse.size() != 0)
                return  produse;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
